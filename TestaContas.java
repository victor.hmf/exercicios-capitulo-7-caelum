public class TestaContas 

{
	public static void main(String[] args) 
	{
		Conta conta1 = new Conta();
		ContaCorrente contaCorrente1 = new ContaCorrente();
		ContaPoupanca contaPoupanca1 = new ContaPoupanca();
	
		conta1.deposita(1500);
		contaCorrente1.deposita(1500);
		contaPoupanca1.deposita(1500);
		
		conta1.atualiza(0.01);
		contaCorrente1.atualiza(0.01);
		contaPoupanca1.atualiza(0.01);

	
		System.out.println(conta1.getSaldo());
		System.out.println(contaCorrente1.getSaldo());
		System.out.println(contaPoupanca1.getSaldo());

	}
}
